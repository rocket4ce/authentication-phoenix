# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :authentication,
  ecto_repos: [Authentication.Repo]

# Configures the endpoint
config :authentication, AuthenticationWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EkYd2Ga242HGN9fV401L8p3JG1+g4lZI2qtFUbh2g5RhCLIhiUeu9TNCx4onaaVD",
  render_errors: [view: AuthenticationWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Authentication.PubSub,
  live_view: [signing_salt: "+aKOP7M3"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
